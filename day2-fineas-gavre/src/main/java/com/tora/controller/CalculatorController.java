package com.tora.controller;

import com.tora.exceptions.DivisionByZeroException;
import com.tora.models.Operation;
import com.tora.models.OperationType;

public class CalculatorController {
    public double computeOperation(Operation operation) {
        switch (operation.getOperationType()) {
            case ADDITION:
                return operation.getOperand1() + operation.getOperand2();
            case SUBTRACTION:
                return operation.getOperand1() - operation.getOperand2();
            case MULTIPLICATION:
                return operation.getOperand1() * operation.getOperand2();
            case DIVISION:
                guardAgainstDivisionByZero(operation);
                return operation.getOperand1() / operation.getOperand2();
            default:
                return 0;
        }
    }

    private void guardAgainstDivisionByZero(Operation operation) {
        if (operation.getOperationType() == OperationType.DIVISION && operation.getOperand2() == 0) {
            throw new DivisionByZeroException();
        }
    }
}
