package com.tora.exceptions;

public class DivisionByZeroException extends RuntimeException {
    public DivisionByZeroException() {
        super("Division by Zero attempted.");
    }
}
