package com.tora.exceptions;

public class InvalidInputException extends RuntimeException {
    public InvalidInputException() {
        super("Invalid input entered by user.");
    }
}
