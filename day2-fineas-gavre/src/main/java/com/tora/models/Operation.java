package com.tora.models;

public class Operation {
    private final double operand1;
    private final double operand2;
    private final OperationType operationType;

    public Operation(double operand1, double operand2, OperationType operationType) {
        this.operand1 = operand1;
        this.operand2 = operand2;
        this.operationType = operationType;
    }

    public double getOperand1() {
        return operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public OperationType getOperationType() {
        return operationType;
    }
}
