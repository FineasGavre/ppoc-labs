package com.tora.view;

import com.tora.controller.CalculatorController;
import com.tora.exceptions.InvalidInputException;
import com.tora.models.Operation;
import com.tora.models.OperationType;

import java.util.Scanner;

public class CommandLineView {
    private final Scanner scanner = new Scanner(System.in);
    private final CalculatorController calculatorController = new CalculatorController();

    public void execute() {
        while (true) {
            System.out.println("Let's calculate something interesting!");
            double operand1 = getDoubleFromInput();
            OperationType operationType = getOperationTypeFromInput();
            double operand2 = getDoubleFromInput();

            Operation operation = new Operation(operand1, operand2, operationType);

            try {
                double result = calculatorController.computeOperation(operation);
                System.out.println("Result is: " + result);
            } catch (Exception ex) {
                System.out.println("There was an error while attempting to compute the result. Please try again!");
            }
        }
    }

    public double getDoubleFromInput() {
        System.out.println("Enter number: ");
        try {
            double input = scanner.nextDouble();
            scanner.nextLine();
            return input;
        } catch (Exception ex) {
            System.out.println("Invalid input, please try again!");
            return getDoubleFromInput();
        }
    }

    public OperationType getOperationTypeFromInput() {
        System.out.println("Enter operation type (+ - * /): ");
        try {
            String input = scanner.nextLine();
            return mapOperationTypeFromString(input);
        } catch (Exception ex) {
            System.out.println("Invalid input, please try again!");
            return getOperationTypeFromInput();
        }
    }

    public OperationType mapOperationTypeFromString(String input) {
        switch (input.trim()) {
            case "+":
                return OperationType.ADDITION;
            case "-":
                return OperationType.SUBTRACTION;
            case "*":
                return OperationType.MULTIPLICATION;
            case "/":
                return OperationType.DIVISION;
            default:
                throw new InvalidInputException();
        }
    }
}
