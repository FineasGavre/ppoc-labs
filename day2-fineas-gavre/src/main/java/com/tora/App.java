package com.tora;

import com.tora.view.CommandLineView;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        new CommandLineView().execute();
    }
}
