package com.tora;

import com.tora.controller.CalculatorController;
import com.tora.models.Operation;
import com.tora.models.OperationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private final CalculatorController calculatorController = new CalculatorController();

    @Test
    public void additionTest() {
        Operation operation = new Operation(1, 2, OperationType.ADDITION);
        double result = calculatorController.computeOperation(operation);
        assertEquals(3, result, 0.01);
    }

    @Test
    public void subtractionTest() {
        Operation operation = new Operation(8, 4, OperationType.SUBTRACTION);
        double result = calculatorController.computeOperation(operation);
        assertEquals(4, result, 0.01);
    }

    @Test
    public void multiplicationTest() {
        Operation operation = new Operation(2, 3, OperationType.MULTIPLICATION);
        double result = calculatorController.computeOperation(operation);
        assertEquals(6, result, 0.01);
    }

    @Test
    public void divisionTest_success() {
        Operation operation = new Operation(6, 3, OperationType.DIVISION);
        double result = calculatorController.computeOperation(operation);
        assertEquals(2, result, 0.01);
    }

    @Test
    public void divisionTest_error() {
        Operation operation = new Operation(6, 0, OperationType.DIVISION);
        boolean hasThrown = false;

        try {
            calculatorController.computeOperation(operation);
        } catch (Exception e) {
            hasThrown = true;
        }

        assertTrue(hasThrown);
    }
}
