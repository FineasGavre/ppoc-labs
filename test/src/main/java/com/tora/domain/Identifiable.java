package com.tora.domain;

public interface Identifiable {
    int getId();
}
