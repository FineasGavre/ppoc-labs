package com.tora.repository;

import com.tora.domain.Identifiable;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final Set<T> set = new TreeSet<>();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
