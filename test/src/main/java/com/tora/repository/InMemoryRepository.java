package com.tora.repository;

import com.tora.domain.Identifiable;

public interface InMemoryRepository<T extends Identifiable> {
    void add(T item);

    boolean contains(T item);

    void remove(T item);
}
