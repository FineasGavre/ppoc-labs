package com.tora.repository;

import com.tora.domain.Identifiable;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<Integer, T> map = new ConcurrentHashMap<>();

    @Override
    public void add(T item) {
        map.put(item.getId(), item);
    }

    @Override
    public boolean contains(T item) {
        return map.contains(item);
    }

    @Override
    public void remove(T item) {
        map.remove(item.getId(), item);
    }
}
