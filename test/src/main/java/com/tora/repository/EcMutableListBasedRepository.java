package com.tora.repository;

import com.tora.domain.Identifiable;
import org.eclipse.collections.api.factory.Lists;
import org.eclipse.collections.api.list.MutableList;

public class EcMutableListBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final MutableList<T> list = Lists.mutable.empty();

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public boolean contains(T item) {
        return list.contains(item);
    }

    @Override
    public void remove(T item) {
        list.remove(item);
    }
}
