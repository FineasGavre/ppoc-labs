package com.tora.repository;

import com.koloboke.collect.set.ObjSet;
import com.koloboke.collect.set.hash.HashObjSets;
import com.tora.domain.Identifiable;

public class ObjSetBasedRepository<T extends Identifiable> implements InMemoryRepository<T> {
    private final ObjSet<T> set = HashObjSets.newMutableSet();

    @Override
    public void add(T item) {
        set.add(item);
    }

    @Override
    public boolean contains(T item) {
        return set.contains(item);
    }

    @Override
    public void remove(T item) {
        set.remove(item);
    }
}
