/*
 * Copyright (c) 2005, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.tora;

import com.tora.domain.Order;
import com.tora.repository.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.ThreadLocalRandom;

@Warmup(iterations = 5, time = 2)
@Measurement(iterations = 5, time = 2, batchSize=100)
@Fork(1)
public class Benchmarks {
    @State(Scope.Benchmark)
    public static class OrderState {
        public Order generateOrder() {
            int id = ThreadLocalRandom.current().nextInt(1, 1_000_000 + 1);
            int price = ThreadLocalRandom.current().nextInt(1, 1_000_000 + 1);
            int quantity = ThreadLocalRandom.current().nextInt(1, 1_000_000 + 1);

            return new Order(id, price, quantity);
        }
    }

    @State(Scope.Benchmark)
    public static class ArrayListState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new ArrayListBasedRepository<>();
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new HashSetBasedRepository<>();
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new TreeSetBasedRepository<>();
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new ConcurrentHashMapBasedRepository<>();
        }
    }

    @State(Scope.Benchmark)
    public static class EcMutableListState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new EcMutableListBasedRepository<>();
        }
    }

    @State(Scope.Benchmark)
    public static class ObjSetState {
        public InMemoryRepository<Order> repository;

        @Setup(Level.Iteration)
        public void setup() {
            repository = new HashSetBasedRepository<>();
        }
    }

    // ArrayList

    @Benchmark
    public void add_ArrayList(ArrayListState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_ArrayList(ArrayListState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_ArrayList(ArrayListState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    // HashSet

    @Benchmark
    public void add_HashSet(HashSetState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_HashSet(HashSetState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_HashSet(HashSetState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    // TreeSet

    @Benchmark
    public void add_TreeSet(TreeSetState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_TreeSet(TreeSetState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_TreeSet(TreeSetState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    // ConcurrentHashMap

    @Benchmark
    public void add_ConcurrentHashMap(ConcurrentHashMapState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_ConcurrentHashMap(ConcurrentHashMapState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_ConcurrentHashMap(ConcurrentHashMapState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    // EcMutableList

    @Benchmark
    public void add_EcMutableList(EcMutableListState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_EcMutableList(EcMutableListState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_EcMutableList(EcMutableListState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    // ObjSet

    @Benchmark
    public void add_ObjSet(ObjSetState repositoryState, OrderState domainState) {
        addItem(domainState.generateOrder(), repositoryState.repository);
    }

    @Benchmark
    public void contains_ObjSet(ObjSetState repositoryState, OrderState domainState, Blackhole blackhole) {
        blackhole.consume(containsItem(domainState.generateOrder(), repositoryState.repository));
    }

    @Benchmark
    public void remove_ObjSet(ObjSetState repositoryState, OrderState domainState) {
        removeItem(domainState.generateOrder(), repositoryState.repository);
    }

    private void addItem(Order order, InMemoryRepository<Order> repository) {
        repository.add(order);
    }

    private boolean containsItem(Order order, InMemoryRepository<Order> repository) {
        return repository.contains(order);
    }

    private void removeItem(Order order, InMemoryRepository<Order> repository) {
        repository.remove(order);
    }
}
